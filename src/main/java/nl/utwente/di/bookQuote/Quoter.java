package nl.utwente.di.bookQuote;

import java.util.HashMap;

public class Quoter {

    private final HashMap<String, Integer> prices = new HashMap<>();

    public Quoter() {
        prices.put("1", 10);
        prices.put("2", 45);
        prices.put("3", 20);
        prices.put("4", 35);
        prices.put("5", 50);
    }

    public double getBookPrice(String isbn) {
        return prices.getOrDefault(isbn, 0);
    }

}
